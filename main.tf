module "user_queue" {
  source  = "terraform-aws-modules/sqs/aws"
  version = "~> 2.0"


  tags = {
    Service     = "user"
    Environment = "dev"
  }
}
